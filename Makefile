start:
	cargo lambda new new-lambda-project
	cd new-lambda-project

watch:
	cargo lambda watch

invoke:
	cargo lambda invoke --data-file input.json

build:
	cargo lambda build --release --arm64

deploy:
	cargo lambda deploy --region us-east-1

aws-invoke:
	# aws lambda invoke --function-name new-lambda-project --payload "{ \"command\": \"hi\" }" response.json
	# cargo lambda invoke --remote new-lambda-project --data-ascii "{ \"command\": \"hi\" }"
	cargo lambda invoke --remote new-lambda-project --data-file input.json

all:
	make start watch invoke build deploy