# Letter Counts

## Name
Letter Counts Cargo Lambda Tool

## Description
This is a cargo lambda tool that takes in a json in the format {"input string": "any text"}, and it returns the letter counts.

## Example Input

#### Input Test into Lambda Function:

![alt text](image-1.png)

#### Output from Test:

![alt text](image-2.png)

#### Input from terminal, input.json:

![Alt text](image-4.png)

#### Output using the terminal:

![Alt text](image-3.png)

#### API Gateway
```
    https://9m9yqvewuk.execute-api.us-east-1.amazonaws.com/default/new-lambda-project
```
![Alt text](image-5.png)

## Installation

For cargo lambda, refer to the [website](https://www.cargo-lambda.info/guide/what-is-cargo-lambda.html).

```
cd new-lambda-project
cargo lambda watch
cargo lambda build --release
cargo lambda invoke --data-file some_json.json 
cargo lambda invoke --remote new-lambda-project --data-file input.json # alternative
```

The Makefile will run all the necessary commands, with make all.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Steps to Setup

    1. Make a new cargo lambda project.

    2. Ensure that your ports for the "watch" command are free (solutions available online).

    3. Install awscli and configure credentials.

    4. Build and deploy the cargo lambda.

    5. Set up an api gateway from AWS management console.

    Bonus : Make a Makefile.

## Authors and acknowledgment
Big thanks to the cloud computing coursera course, from Noah Gift. 

## License
Creative Commons

## Project status
This project has been completed, as of February 2024.
